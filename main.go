package main

import (
	"log"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type Breakout struct {}

func (b *Breakout) Update() error {
	return nil
}

func (b *Breakout) Draw(tela *ebiten.Image) {
	ebitenutil.DebugPrint(tela, "Olá, mundo!")
}

func (b *Breakout) Layout(largura, altura int) (telaLargura, telaAltura int) {
	telaLargura = largura
	telaAltura = altura
	return
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Breakout")
	if err := ebiten.RunGame(&Breakout{}); err != nil {
		log.Fatal(err)
	}
}
